package function;

public class Function {

    /**
     * @param args the command line arguments
     */
    static int f(int k){
        return 5*k+10;
    }
    public static void main(String[] args) {
        System.err.println(f(10));
    }
    
}
